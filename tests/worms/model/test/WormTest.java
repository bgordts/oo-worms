package worms.model.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import worms.model.IllegalMovementException;
import worms.model.Worm;
import worms.util.Util;

/**
 * TODO:
 *  - Illegal case for the constructor
 *  - Test for action points in constructor
 *  - Tests with big numbers
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * 
 * Repository: https://bitbucket.org/bgordts/oo-worms/overview
 *
 */

public class WormTest {
	
	//A worm at the origin with direction 0 degrees and radius 0.50.
	private static Worm worm0X0Y0Direction50Radius;
	//A worm at the origin with direction 90 degrees and radius 0.50.
	private static Worm worm0X0Y90Direction50Radius;
	//A worm at the origin with direction 135 degrees and radius 0.50.
	private static Worm worm0X0Y135Direction50Radius;
	//A worm at the origin with direction 200 degrees and radius 0.50.
	private static Worm worm0X0Y200Direction50Radius;
	//A worm at the origin with direction 45 degrees, radius 0.50 and 100 action points.
	private static Worm worm0X0Y45Direction50Radius100AP;
	//A worm with direction 150 degrees, radius 0.50 and 200 action points.
	private static Worm worm150Direction50Radius200AP;
	//A worm with 0 action points
	private static Worm wormZeroActionPoints;
	//A worm with max action points
	private static Worm wormMaxActionPoints;

	/**
	 * Set up the mutable fixture. These are variables that will be the same for each test.
	 * 
	 */
	@Before
	public void setUpMutableFixture(){
		worm0X0Y0Direction50Radius = new Worm(0.0,0.0, 0.0, 0.50, "Guido");
		worm0X0Y90Direction50Radius = new Worm(0.0,0.0, Math.PI/2, 0.50, "Sammy");
		worm0X0Y135Direction50Radius = new Worm(0.0,0.0, Math.PI - Math.PI/4, 0.50, "Lydia Protut");
		wormZeroActionPoints = new Worm(0.0,0.0, 0.0, 0.50, "Anita",0);
		worm0X0Y200Direction50Radius = new Worm(0.0,0.0, 3.490658504, 0.50, "Michel");
		wormMaxActionPoints = new Worm(0.0,0.0, 0.0, 0.50, "Alain", 0, true);
		worm0X0Y45Direction50Radius100AP = new Worm(0.0,0.0, Math.PI/4.0, 0.50, "Franky", 200);
		worm150Direction50Radius200AP = new Worm(99.0,-36.899, Math.PI - Math.PI/6.0, 0.50, "Hanz", 200);
	}
	
	@Test
	public void constructor_smallSingleCase() {
		Worm  worm = new Worm(0.0,0.0, Math.PI, 0.50, "Legit Worm");
		
		//Using the fuzzyEquals method from the Util library to compare doubles. The default epsilon, 1e-4, is
		//precise enough for this tests (0.1 mm)
		assertTrue(Util.fuzzyEquals(worm.getPositionX(), 0.0));
		assertTrue(Util.fuzzyEquals(worm.getPositionY(), 0.0));
		assertTrue(Util.fuzzyEquals(worm.getDirection(), Math.PI));
		assertTrue(Util.fuzzyEquals(worm.getRadius(), 0.50));
		assertTrue(Util.fuzzyEquals(worm.getRadiusLowerBound(), 0.25));
		assertEquals(worm.getName(), "Legit Worm");
	}
	
	@Test
	public void constructor_mediumSingleCase() {
		Worm  worm = new Worm(0.0,0.0, Math.PI, 0.50, "Legit Worm", 200);
		
		assertTrue(Util.fuzzyEquals(worm.getPositionX(), 0.0));
		assertTrue(Util.fuzzyEquals(worm.getPositionY(), 0.0));
		assertTrue(Util.fuzzyEquals(worm.getDirection(), Math.PI));
		assertTrue(Util.fuzzyEquals(worm.getRadius(), 0.50));
		assertTrue(Util.fuzzyEquals(worm.getRadiusLowerBound(), 0.25));
		assertEquals(worm.getName(), "Legit Worm");
		assertTrue(worm.getActionPointsCurrent() == 200);
	}
	
	@Test
	public void constructor_largeTrueCase() {
		Worm  worm = new Worm(0.0,0.0, Math.PI, 0.50, "Legit Worm", 0, true);
		
		assertTrue(Util.fuzzyEquals(worm.getPositionX(), 0.0));
		assertTrue(Util.fuzzyEquals(worm.getPositionY(), 0.0));
		assertTrue(Util.fuzzyEquals(worm.getDirection(), Math.PI));
		assertTrue(Util.fuzzyEquals(worm.getRadius(), 0.50));
		assertTrue(Util.fuzzyEquals(worm.getRadiusLowerBound(), 0.25));
		assertEquals(worm.getName(), "Legit Worm");
		assertTrue(worm.getActionPointsCurrent() == worm.getActionPointsMax());
	}
	
	@Test
	public void constructor_largeFalseCase() {
		Worm  worm = new Worm(0.0,0.0, Math.PI, 0.50, "Legit Worm", 0, false);
		
		assertTrue(Util.fuzzyEquals(worm.getPositionX(), 0.0));
		assertTrue(Util.fuzzyEquals(worm.getPositionY(), 0.0));
		assertTrue(Util.fuzzyEquals(worm.getDirection(), Math.PI));
		assertTrue(Util.fuzzyEquals(worm.getRadius(), 0.50));
		assertTrue(Util.fuzzyEquals(worm.getRadiusLowerBound(), 0.25));
		assertEquals(worm.getName(), "Legit Worm");
		assertTrue(worm.getActionPointsCurrent() == 0);
	}

	@Test
	public void move_5StepsHorizontalCase() {
		int actionPointsBeforeMove = worm0X0Y0Direction50Radius.getActionPointsCurrent();
		
		worm0X0Y0Direction50Radius.move(5);
		
		assertTrue(Util.fuzzyEquals(worm0X0Y0Direction50Radius.getPositionX(), 2.50));
		assertTrue(Util.fuzzyEquals(worm0X0Y0Direction50Radius.getPositionY(), 0.0));
		
		//5 steps in horizontal direction cost 5 action points
		assertTrue(worm0X0Y0Direction50Radius.getActionPointsCurrent() == actionPointsBeforeMove - 5);
	}
	
	@Test
	public void move_5StepsVerticalCase() {
		int actionPointsBeforeMove = worm0X0Y90Direction50Radius.getActionPointsCurrent();
		
		worm0X0Y90Direction50Radius.move(5);
		
		assertTrue(Util.fuzzyEquals(worm0X0Y90Direction50Radius.getPositionX(), 0.0));
		assertTrue(Util.fuzzyEquals(worm0X0Y90Direction50Radius.getPositionY(), 2.50));
		
		//5 steps in vertical direction cost 4*5 = 20 action points
		assertTrue(worm0X0Y90Direction50Radius.getActionPointsCurrent() == actionPointsBeforeMove - 20);
	}
	
	@Test
	public void move_6StepsSkewCase() {
		int actionPointsBeforeMove = worm0X0Y135Direction50Radius.getActionPointsCurrent();
		
		worm0X0Y135Direction50Radius.move(6);
		
		assertTrue(Util.fuzzyEquals(worm0X0Y135Direction50Radius.getPositionX(), -2.1213));
		assertTrue(Util.fuzzyEquals(worm0X0Y135Direction50Radius.getPositionY(), 2.1213));
		
		//6*(-cos(135) + 4*sin(135)) = 21.21 ==> we lose 22 action points
		assertTrue(worm0X0Y135Direction50Radius.getActionPointsCurrent() == actionPointsBeforeMove - 22);
	}
	

	@Test
	public void canMove_trueCase() {
		assertTrue(worm0X0Y0Direction50Radius.canMove(6));
	}
	
	@Test
	public void canMove_MaxBorder() {
		assertTrue(worm0X0Y0Direction50Radius.canMove(worm0X0Y0Direction50Radius.getActionPointsCurrent()));
	}
	
	@Test
	public void canMove_FalseCase() {
		assertFalse(wormZeroActionPoints.canMove(5));
	}
	
	@Test
	public void actionPointCost_HorizontalCase(){
		assertTrue(Util.fuzzyEquals(worm0X0Y0Direction50Radius.actionPointCost(5), 5));
	}
	
	@Test
	public void actionPointCost_VerticalCase(){
		assertTrue(Util.fuzzyEquals(worm0X0Y90Direction50Radius.actionPointCost(5), 20));
	}
	
	@Test
	public void actionPointCost_SkewCase(){
		assertTrue(worm0X0Y135Direction50Radius.actionPointCost(6) == 22);
	}

	@Test
	public void turn_180DegreesCase() {
		int actionPointsBeforeMove = worm0X0Y0Direction50Radius.getActionPointsCurrent();
		
		worm0X0Y0Direction50Radius.turn(Math.PI);
		
		assertTrue(Util.fuzzyEquals(worm0X0Y0Direction50Radius.getDirection(), Math.PI));
		
		assertTrue(worm0X0Y0Direction50Radius.getActionPointsCurrent() == actionPointsBeforeMove - 30);
	}
	
	@Test
	public void turn_269DegreesCase() {
		int actionPointsBeforeMove = worm0X0Y0Direction50Radius.getActionPointsCurrent();
		
		//269 degrees = 4.694935688
		worm0X0Y0Direction50Radius.turn(4.694935688);
		
		assertTrue(Util.fuzzyEquals(worm0X0Y0Direction50Radius.getDirection(), 4.694935688));
		
		//4.694935688*(60/(2*Pi)) = 45
		assertTrue(worm0X0Y0Direction50Radius.getActionPointsCurrent() == actionPointsBeforeMove - 45);
	}
	
	@Test
	public void turn_180DegreesFrom90DegreesCase() {
		int actionPointsBeforeMove = worm0X0Y90Direction50Radius.getActionPointsCurrent();
		
		worm0X0Y90Direction50Radius.turn(Math.PI);
		
		assertTrue(Util.fuzzyEquals(worm0X0Y90Direction50Radius.getDirection(), Math.PI + Math.PI/2));
		
		assertTrue(worm0X0Y90Direction50Radius.getActionPointsCurrent() == actionPointsBeforeMove - 30);
	}
	
	@Test
	public void turn_negativeCase() {
		int actionPointsBeforeMove = worm0X0Y0Direction50Radius.getActionPointsCurrent();
		
		worm0X0Y0Direction50Radius.turn(-Math.PI);
		
		assertTrue(Util.fuzzyEquals(worm0X0Y0Direction50Radius.getDirection(), Math.PI));
		
		assertTrue(worm0X0Y0Direction50Radius.getActionPointsCurrent() == actionPointsBeforeMove - 30);
	}
	
	@Test
	public void turn_540DegreesCase() {
		int actionPointsBeforeMove = worm0X0Y0Direction50Radius.getActionPointsCurrent();
		
		worm0X0Y0Direction50Radius.turn(Math.PI*3);
		
		assertTrue(Util.fuzzyEquals(worm0X0Y0Direction50Radius.getDirection(), Math.PI));
		
		assertTrue(worm0X0Y0Direction50Radius.getActionPointsCurrent() == actionPointsBeforeMove - 90);
	}
	
	@Test
	public void canTurn_trueCase() {
		assertTrue(worm0X0Y0Direction50Radius.canTurn(Math.PI));
	}
	
	@Test
	public void canTurn_falseCase() {
		assertFalse(wormZeroActionPoints.canTurn(Math.PI));
	}
	
	@Test
	public void jump_legalCase() {
		worm0X0Y0Direction50Radius.jump();
		
		assertTrue(Util.fuzzyEquals(worm0X0Y0Direction50Radius.getPositionX(), 0.0));
		
		assertTrue(worm0X0Y0Direction50Radius.getActionPointsCurrent() == 0);
	}
	
	@Test
	public void jump_legal45degreesCase() {
		worm0X0Y45Direction50Radius100AP.jump();
		
		//Calculated in Maple
		assertTrue(Util.fuzzyEquals(worm0X0Y45Direction50Radius100AP.getPositionX(), 3.433289627));
		
		assertTrue(worm0X0Y45Direction50Radius100AP.getActionPointsCurrent() == 0);
	}
	
	@Test
	public void jump_legal150degreesNotOriginCase() {
		worm150Direction50Radius200AP.jump();
		
		//Calculated in Maple
		assertTrue(Util.fuzzyEquals(worm150Direction50Radius200AP.getPositionX(), 96.02668396));
		
		assertTrue(worm150Direction50Radius200AP.getActionPointsCurrent() == 0);
	}
	
	@Test(expected = IllegalMovementException.class)
	public void jump_illegalCase() {	
		worm0X0Y200Direction50Radius.jump();
	}
	
	@Test
	public void jumpTime_45degreesCase() {
		assertTrue(Util.fuzzyEquals(worm0X0Y45Direction50Radius100AP.jumpTime(), 0.8367772814));
	}
	
	@Test
	public void jumpStep_zeroTimeCase() {
		assertTrue(Util.fuzzyEquals(worm0X0Y45Direction50Radius100AP.jumpStep(0)[0], 0.0));
		assertTrue(Util.fuzzyEquals(worm0X0Y45Direction50Radius100AP.jumpStep(0)[1], 0.0));
	}
	
	@Test
	public void jumpStep_endTimeCase() {
		assertTrue(Util.fuzzyEquals(worm0X0Y45Direction50Radius100AP.jumpStep(0.8367772814)[0], 3.433289627));
		assertTrue(Util.fuzzyEquals(worm0X0Y45Direction50Radius100AP.jumpStep(0.8367772814)[1], 0.0));
	}
	
	@Test
	public void jumpStep_betweenCase() {
		assertTrue(Util.fuzzyEquals(worm0X0Y45Direction50Radius100AP.jumpStep(0.5)[0], 2.051495481));
		assertTrue(Util.fuzzyEquals(worm0X0Y45Direction50Radius100AP.jumpStep(0.5)[1], 0.825664231));
	}
	
	@Test
	public void setName_legalSimpleCase(){
		worm0X0Y0Direction50Radius.setName("Jonas");
		
		assertTrue(worm0X0Y0Direction50Radius.getName() == "Jonas");
	}
	@Test
	public void setName_legalQuoteCase(){
		worm0X0Y0Direction50Radius.setName("James o'Hara");
		
		assertTrue(worm0X0Y0Direction50Radius.getName() == "James o'Hara");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setName_illegalDigitCase(){
		worm0X0Y0Direction50Radius.setName("Jonas2");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setName_illegalNoFirstCapitalCase(){
		worm0X0Y0Direction50Radius.setName("jonas");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setName_illegalTooShortCase(){
		worm0X0Y0Direction50Radius.setName("U");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setName_illegalSpecialCharCase(){
		worm0X0Y0Direction50Radius.setName("Boris&Jan");
	}
	
	@Test
	public void setPositionX_legalCase(){
		worm0X0Y0Direction50Radius.setPositionX(999999999);
		
		assertTrue(worm0X0Y0Direction50Radius.getPositionX() == 999999999);
	}
	
	@Test
	public void setPositionX_legalNegativeCase(){
		worm0X0Y0Direction50Radius.setPositionX(-999999999);
		
		assertTrue(worm0X0Y0Direction50Radius.getPositionX() == -999999999);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setPositionX_illegalJCase(){
		worm0X0Y0Direction50Radius.setPositionX(Math.sqrt(-1));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setPositionX_illegalNegativeInfinityCase(){
		worm0X0Y0Direction50Radius.setPositionX(Double.NEGATIVE_INFINITY);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setPositionX_illegalPositiveInfinityCase(){
		worm0X0Y0Direction50Radius.setPositionX(Double.POSITIVE_INFINITY);
	}
	
	@Test
	public void setPositionY_legalCase(){
		worm0X0Y0Direction50Radius.setPositionY(999999999);
		
		assertTrue(worm0X0Y0Direction50Radius.getPositionY() == 999999999);
	}
	
	@Test
	public void setPositionY_legalNegativeCase(){
		worm0X0Y0Direction50Radius.setPositionY(-999999999);
		
		assertTrue(worm0X0Y0Direction50Radius.getPositionY() == -999999999);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setPositionY_illegalJCase(){
		worm0X0Y0Direction50Radius.setPositionY(Math.sqrt(-1));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setPositionY_illegalNegativeInfinityCase(){
		worm0X0Y0Direction50Radius.setPositionY(Double.NEGATIVE_INFINITY);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setPositionY_illegalPositiveInfinityCase(){
		worm0X0Y0Direction50Radius.setPositionY(Double.POSITIVE_INFINITY);
	}
	
	@Test
	public void setPositionY_singleCase(){
		worm0X0Y0Direction50Radius.setDirection(Math.PI);
		
		assertTrue(worm0X0Y0Direction50Radius.getDirection() == Math.PI);
	}
	
	@Test
	public void setRadius_simpleCase(){
		worm0X0Y0Direction50Radius.setRadius(0.30);
		
		assertTrue(worm0X0Y0Direction50Radius.getRadius() == 0.30);
	}
	
	@Test
	public void setRadius_lessActionPointsCase(){
		wormMaxActionPoints.setRadius(0.26);
		
		assertTrue(wormMaxActionPoints.getRadius() == 0.26);
		assertTrue(wormMaxActionPoints.getActionPointsCurrent() == wormMaxActionPoints.getActionPointsMax());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setRadius_illegalNaNCase(){
		worm0X0Y0Direction50Radius.setRadius(Math.sqrt(-1));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setRadius_illegalTooSmallCase(){
		worm0X0Y0Direction50Radius.setRadius(worm0X0Y0Direction50Radius.getRadiusLowerBound() - 0.02);
	}
	
	@Test
	public void getMass_simpleCase(){		
		assertTrue(Util.fuzzyEquals(worm0X0Y0Direction50Radius.getMass(),556.0618997));
	}
	
	@Test
	public void setActionPointsCurrent_simpleCase(){
		worm0X0Y0Direction50Radius.setActionPointsCurrent(50);
		
		assertTrue(worm0X0Y0Direction50Radius.getActionPointsCurrent() == 50);
	}
	
	@Test
	public void setActionPointsCurrent_negativeCase(){
		worm0X0Y0Direction50Radius.setActionPointsCurrent(-50);
		
		assertTrue(worm0X0Y0Direction50Radius.getActionPointsCurrent() == 0);
	}
	
	@Test
	public void setActionPointsCurrent_tooHighCase(){
		worm0X0Y0Direction50Radius.setActionPointsCurrent(worm0X0Y0Direction50Radius.getActionPointsMax() + 1000);
		
		assertTrue(worm0X0Y0Direction50Radius.getActionPointsCurrent() == worm0X0Y0Direction50Radius.getActionPointsMax());
	}
	
	@Test
	public void getActionPointsMax_singleCase(){
		assertTrue(worm0X0Y0Direction50Radius.getActionPointsMax() == 556);
	}
}
