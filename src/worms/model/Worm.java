package worms.model;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class of worm, with a position, orientation, mass and action points. It can also move and jump.
 * 
 * @invar 	All numbers must be valid
 * 		  | positionX != Float.NaN
 * 		  | positionY != Float.NaN
 * 		  | direction != Float.NaN
 * 		  | radius != Float.NaN
 * 		  | radiusLowerBound != Float.NaN
 * @invar	The name must be valid
 * 		  | isValidName(this.getName())
 * @invar	direction is always between 0 and 2*Pi
 * 		  | 0 <= direction && direction <= 2*Pi
 * @invar 	radius must be larger than the lower bound
 * 		  | radius >= radiusLowerBound
 * @invar 	radiusLowerBound must not be a negative number
 * 		  | radiusLowerBound >= 0
 * @invar	actionPointsCurrent must not be negative and smaller than the maximum
 * 		  | actionPointsCurrent <= this.getActionPointsMax()
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * 
 * Repository: https://bitbucket.org/bgordts/oo-worms/overview
 *
 */
public class Worm {
	
	private String name;
	
	//Position
	private double positionX;
	private double positionY;
	private double direction; //Facing right is 0 radians, up Pi/2 and so on...
	
	//Look and feel
	private double radius;
	private double radiusLowerBound;
	
	//Action Points
	private int actionPointsCurrent;
	
	//Earth's gravitation acceleration
	private final double g = 9.80665;
	
	/**
	 * Constructor for Worm.
	 * 
	 * @param 	x
	 * 			the x value to initialize to
	 * @param 	y
	 * 			the y value to initialize to
	 * @param 	direction
	 * 			the orientation to initialize to
	 * @param 	radius
	 * 			the radius to initialize to
	 * @param 	name
	 * 			the name to initialize to
	 * @effect	the X position will be set to x
	 * 		  | this.setPositionX(x)
	 * @effect	the Y position will be set to y
	 * 		  | this.setPositionY(y)
	 * @effect	the direction will be set to direction
	 * 		  | this.setDirection(direction)
	 * @effect	the radius will be set to radius
	 * 		  | this.setRadius(radius)
	 * @effect	the name position will be set to name
	 * 		  | this.setName(name)
	 * @effect	The lower bound will be set by default to 0.25
	 * 		  | this.setRadiusLowerBound(0.25)
	 * @effect	The current action points will be set by default to the maximum
	 * 		  | this.setActionPointsCurrent(this.getActionPointsMax())
	 */
	@Raw
	public Worm(double x, double y, double direction, double radius, String name){
		this(x, y, direction, radius, name, 0, true);
	}
	
	/**
	 * Constructor for Worm
	 * 
	 * @param 	x
	 * 			the x value to initialize to
	 * @param 	y
	 * 			the y value to initialize to
	 * @param 	direction
	 * 			the orientation to initialize to
	 * @param 	radius
	 * 			the radius to initialize to
	 * @param 	name
	 * 			the name to initialize to
	 * @param	actionPoints
	 * 			the amount of action points to start with
	 * @effect	the X position will be set to x
	 * 		  | this.setPositionX(x)
	 * @effect	the Y position will be set to y
	 * 		  | this.setPositionY(y)
	 * @effect	the direction will be set to direction
	 * 		  | this.setDirection(direction)
	 * @effect	the radius will be set to radius
	 * 		  | this.setRadius(radius)
	 * @effect	the name position will be set to name
	 * 		  | this.setName(name)
	 * @effect	The lower bound will be set by default to 0.25
	 * 		  | this.setRadiusLowerBound(0.25)
	 * @effect	The current action points will be set to actionPoints
	 * 		  | this.setActionPointsCurrent(actionPoints)
	 */
	@Raw
	public Worm(double x, double y, double direction, double radius, String name, int actionPoints){
		this(x, y, direction, radius, name, actionPoints, false);
	}
	
	/**
	 * Constructor for Worm
	 * 
	 * @param 	x
	 * 			the x value to initialize to
	 * @param 	y
	 * 			the y value to initialize to
	 * @param 	direction
	 * 			the orientation to initialize to
	 * @param 	radius
	 * 			the radius to initialize to
	 * @param 	name
	 * 			the name to initialize to
	 * @param	actionPoints
	 * 			the amount of action points to start with, if maxActionPoints is set to false
	 * @param	maxActionPoints
	 * 			true: set the action points to the maximum amount, false: set the action points to actionPoints
	 * @effect	the X position will be set to x
	 * 		  | this.setPositionX(x)
	 * @effect	the Y position will be set to y
	 * 		  | this.setPositionY(y)
	 * @effect	the direction will be set to direction
	 * 		  | this.setDirection(direction)
	 * @effect	the radius will be set to radius
	 * 		  | this.setRadius(radius)
	 * @effect	the name position will be set to name
	 * 		  | this.setName(name)
	 * @effect	The lower bound will be set by default to 0.25
	 * 		  | this.setRadiusLowerBound(0.25)
	 * @effect	The current action points will be set to actionPoints if maxActionPoints is false, otherwise actionPoints is set to
	 * 			the maximum action points possible
	 * 		  | if(maxActionPoints)
	 * 		  |		this.setActionPointsCurrent((this.getActionPointsMax())
	 * 		  | else
	 * 		  |		this.setActionPointsCurrent(actionPoints)
	 */
	@Raw
	public Worm(double x, double y, double direction, double radius, String name, int actionPoints, boolean maxActionPoints){
		this.setPositionX(x);
		this.setPositionY(y);
		this.setDirection(direction);
		this.setRadius(radius);
		this.setRadiusLowerBound(0.25);
		this.setName(name);
		
		if(maxActionPoints){
			this.setActionPointsCurrent(this.getActionPointsMax());
		} else{
			this.setActionPointsCurrent(actionPoints);
		}
		
	}

	
	/**
	 * Change the position of the Worm.
	 * 
	 * @param 	numberOfSteps
	 * 			the number of steps to take
	 * @effect	set both x and y positions to the right values
	 * 		  | new.getPositionX() == this.GetPositionX() + horizontalStepDistance*numberOfSteps
	 * 		  | new.getPositionY() == this.GetPositionY() + verticalStepDistance*numberOfSteps
	 * @effect	decrease the current action points by the correct amount
	 * 		  | this.setActionPointsCurrent(this.getActionPointsCurrent() - actionPointCost(numberOfSteps))
	 * @throws	IllegalMovementException
	 * 			the worm must have enough action points for this movement
	 * 		  | !canMove(numberOfSteps)
	 * 		  
	 */
	public void move(int numberOfSteps) throws IllegalMovementException{
		if(!canMove(numberOfSteps))
			throw new IllegalMovementException("Not enough action points for this movement");
		
		double horizontalStepDistance = this.getRadius()*Math.cos(this.getDirection());
		double verticalStepDistance = this.getRadius()*Math.sin(this.getDirection());
		
		this.setPositionX(this.getPositionX() + horizontalStepDistance*numberOfSteps);
		this.setPositionY(this.getPositionY() + verticalStepDistance*numberOfSteps);
		this.setActionPointsCurrent(this.getActionPointsCurrent() - actionPointCost(numberOfSteps));
		
	}
	
	/**
	 * Check if the worm can move the given amount of steps
	 * 
	 * @param 	numberOfSteps
	 * 			the number of steps to take
	 * @return  whether it's possible to move the number of steps
	 * 		  | this.actionPointCost(numberOfSteps) <= this.getActionPointsCurrent();
	 */
	public boolean canMove(int numberOfSteps){
		return this.actionPointCost(numberOfSteps) <= this.getActionPointsCurrent();
	}
	
	/**
	 * Calculate the amount it would cost to move the given amount of steps
	 * 
	 * @param 	numberOfSteps
	 * 			the number of steps to take
	 * @return  the amount of action points it would cost is given by the following formula
	 * 		  | result == (int) Math.ceil(Math.abs(horizontalStepCost*numberOfSteps) + Math.abs(4*verticalStepCost*numberOfSteps))
	 */
	public int actionPointCost(int numberOfSteps){
		double horizontalStepCost = Math.cos(this.getDirection());
		double verticalStepCost = Math.sin(this.getDirection());
		
		return (int) Math.ceil(numberOfSteps*(Math.abs(horizontalStepCost) + Math.abs(4*verticalStepCost)));
	}
	
	/**
	 * Change the orientation of the worm over the given angle
	 * 
	 * @param 	angle
	 * 			the angle to turn over
	 * @pre		the angle must be a valid number
	 * 		  | angle != Float.NaN
	 * @pre		the worm must have enough action points to turn the given angle
	 * 		  | canTurn(angle)
	 * @effect	the direction will be increased by angle modulus 2*Pi
	 * 		  | this.getDirection() == (this.getDirection() + angle) % (2*Math.PI)
	 */
	public void turn(double angle){
		assert(angle != Float.NaN);
		assert(canTurn(angle));
		
		//Turn
		double direction = (this.getDirection() + angle) % (2*Math.PI);
		direction = (direction < 0)?direction+2*Math.PI:direction;
		this.setDirection(direction);
		
		//Calculate used action points
		int actionPointsUsed = (int) Math.ceil(Math.abs(60*(angle/(2*Math.PI))));
		this.setActionPointsCurrent(this.getActionPointsCurrent() - actionPointsUsed);
	}
	
	/**
	 * Check if the worm can turn the given angle
	 * 
	 * @param 	angle
	 * 			the angle to turn over
	 * @return	the action points needed must not exceed the current amount the worm has
	 * 		  | this.getActionPointsCurrent() >= (int) Math.ceil(Math.abs(60*(angle/(2*Math.PI))));
	 */
	public boolean canTurn(double angle){
		return this.getActionPointsCurrent() >= (int) Math.ceil(Math.abs(60*(angle/(2*Math.PI))));
	}
	
	/**
	 * Let the worm Jump.
	 * 
	 * @post	the current amount of action points is set to 0
	 * 		  | this.getActionPointsCurrent() == 0
	 * @effect	the x position will change with the calculated jump distance
	 * 		  |	if(this.getDirection() <= Math.PI)
	 *    	  | 	this.setPositionX(this.getPositionX() + this.calculateJumpDistance());
	 * @throws	IllegalMovementException
	 * 			The worm must face up to jump
	 * 		  | this.getDirection > Math.PI
	 */
	public void jump() throws IllegalMovementException {
		//We only have to check this case, because in the turn() and setDirection() methods you can't set the direction negative
		//or above 2 times Pi
		if(this.getDirection() > Math.PI)
			throw new IllegalMovementException("The worm must face up to jump");
			
		this.setPositionX(this.getPositionX() + this.calculateJumpDistance());

		this.setActionPointsCurrent(0);
		
	}
	
	/**
	 * Time needed to complete the jump.
	 * 
	 * @return the time calculated
	 * 		 | result == this.calculateJumpDistance()/(this.calculateInitialJumpSpeed()*Math.cos(this.getDirection()))
	 */
	public double jumpTime(){
		return this.calculateJumpDistance()/(this.calculateInitialJumpSpeed()*Math.cos(this.getDirection()));
	}
	
	/**
	 * Returns the in-jump positions from a jump launched at the current position.
	 * 
	 * @param	timeStep
	 * 			the time at which to calculate the position
	 * @return	An array of two doubles containing the x and y position of the 
	 * 			worm at that point in time
	 * 		  | newPosition[0] == x + (vX*timeStep);
	 * 		  | newPosition[1] == y + (vY*timeStep -0.5*g*timeStep*timeStep);
	 */
	public double[] jumpStep(double timeStep){		
		double v = calculateInitialJumpSpeed();
		
		double x = this.getPositionX();
		double y = this.getPositionY();
		double vX = v*Math.cos(this.getDirection());
		double vY = v*Math.sin(this.getDirection());
		
		x = x + (vX*timeStep);
		y = y + (vY*timeStep -0.5*g*timeStep*timeStep);
		
		double[] newPosition = {x,y};
		
		return newPosition;
	}
	
	/**
	 * Calculate the force F if this worm were to jump
	 * 
	 * @return	the force of the jump
	 * 		  | result == 5*this.getActionPointsCurrent() + this.getMass()*g
	 */
	private double calculateJumpForce(){
		return 5*this.getActionPointsCurrent() + this.getMass()*g;
	}
	
	/**
	 * Calculate the initial speed of this worm were it to jump
	 * 
	 * @return the initial speed of the jump
	 * 		 | result == (this.calculateJumpForce()/this.getMass())*0.5
	 */
	private double calculateInitialJumpSpeed(){
		return (this.calculateJumpForce()/this.getMass())*0.5;
	}
	
	/**
	 * Calculate the distance this worm would travel were it to jump
	 * 
	 * @return the distance of the jump
	 * 		 | result == (Math.pow(calculateInitialJumpSpeed(), 2) * Math.sin(2*this.getDirection())) / g
	 */
	private double calculateJumpDistance(){
		return (Math.pow(calculateInitialJumpSpeed(), 2) * Math.sin(2*this.getDirection())) / g;
	}
	
	/**
	 * Get the name of the worm
	 * 
	 * @return the name of the worm
	 * 		 | result == this.name
	 */
	@Basic
	public String getName() {
		return name;
	}
	
	/**
	 * Set the name of the worm
	 * 
	 * @param 	name
	 * 			the name to set to
	 * @post	the name of the worm is set to name
	 * 		  | this.getName() == name
	 * @throws	IllegalArgumentException
	 * 			The name of the worm must be valid: it cannot be null, it 
	 * 			must be at least 2 characters long, it must start with a capital letter and must 
	 * 			only consist of letters, quotes and spaces
	 * 		  | !Worm.isValidName(name)
	 */
	@Raw
	public void setName(String name) throws IllegalArgumentException {
		if(!Worm.isValidName(name))
			throw new IllegalArgumentException("the name given was invalid");	
		this.name = name;
	}
	
	/**
	 * Check if a given name is valid
	 * 
	 * @param 	name
	 * 			name to check
	 * @return	returns true if all the conditions for a valid name are met: it cannot be null, it 
	 * 			must be at least 2 characters long, it must start with a capital letter and must 
	 * 			only consist of letters, quotes and spaces
	 */
	private static boolean isValidName(String name){
		if(name == null)
			return false;
		if(name.length() < 2)
			return false;
		if(!Character.isUpperCase(name.charAt(0)))
			return false;
		for(int i = 0; i < name.length(); i++)
			if(!(Character.isLetter(name.charAt(i)) || name.charAt(i) == ' ' || name.charAt(i) == '\'' || name.charAt(i) == '\"'))
				return false;
		return true;
	}
	
	/** 
	 * Get the x position of the worm
	 * 
	 * @return	the x position of this worm
	 * 		  | result == this.positionX
	 */
	@Basic
	public double getPositionX() {
		return positionX;
	}
	
	/** 
	 * Set the x position of the worm
	 * 
	 * @param 	posititionX
	 * 			the position to set to
	 * @post	change the x position of the worm to positionX
	 * 		  | new.getPositionX() == positionX
	 * @throws	IllegalArgumentException
	 * 			the position must be a valid number
	 * 		  | positionX == Float.NaN
	 * 			
	 */
	@Raw
	public void setPositionX(double posititionX) throws IllegalArgumentException {
		if(Double.isNaN(posititionX) || Double.isInfinite(posititionX))
			throw new IllegalArgumentException("The x position must be a valid number");
		this.positionX = posititionX;
	}
	
	/** 
	 * Get the y position of the worm
	 * 
	 * @return	the y position of this worm
	 * 		  | result == this.positionY
	 */
	@Basic
	public double getPositionY() {
		return positionY;
	}
	
	/** 
	 * Set the y position of the worm
	 * 
	 * @param 	posititionY
	 * 			the position to set to
	 * @post	change the Y position of the worm to positionY
	 * 		  | new.getPositionY() == positionY
	 * @throws	IllegalArgumentException
	 * 			the position must be a valid number
	 * 		  | positionY == Float.NaN
	 * 			
	 */
	@Raw
	public void setPositionY(double positionY) {
		if(Double.isNaN(positionY) || Double.isInfinite(positionY))
			throw new IllegalArgumentException("The y position must be a valid number");
		this.positionY = positionY;
	}
	
	/**
	 * Get the direction of the worm
	 * 
	 * @return the direction of this worm
	 * 		 | result == this.direction
	 */
	@Basic
	public double getDirection() {
		return direction;
	}
	
	/**
	 * Set the direction the worm faces
	 * 
	 * @param 	direction
	 * 			the direction to set to
	 * @pre		the direction must be a valid number
	 * 		  | direction != Float.NaN
	 * @pre		the direction must be between 0 and 2*Pi
	 * 		  | 0 <= direction && direction <= 2*Math.PI
	 * @post	change the direction the worm is facing
	 * 		  | this.direction == direction
	 */
	@Raw
	public void setDirection(double direction) {
		assert(direction != Float.NaN);
		assert(0 <= direction && direction <= 2*Math.PI);
		this.direction = direction;
	}
	
	/**
	 * Get the current radius of the worm
	 * 
	 * @return	the current radius of this worm
	 * 		  | result == this.radius
	 */
	@Basic
	public double getRadius() {
		return radius;
	}
	
	/**
	 * Set the current radius of the worm
	 * 
	 * @param 	radius
	 * 			the radius to set to
	 * @post	the radius of this worm is set to radius
	 * 		  | this.radius == radius
	 * @post	changing the radius changes the max amount of AP, thus we need to reset the action points
	 * 		  | this.setActionPointsCurrent(this.getActionPointsCurrent())
	 * @throws	IllegalArgumentException
	 * 			the radius must be a valid number
	 * 		  | radius == Float.NaN
	 *			the radius must be larger than or equal to the lower bound
	 * 		  | radius < this.getRadiusLowerBound()
	 * 			
	 */
	@Raw
	public void setRadius(double radius) throws IllegalArgumentException {
		if(Double.isNaN(radius))
			throw new IllegalArgumentException("The radius must be a valid number");
		if(radius < this.getRadiusLowerBound())
			throw new IllegalArgumentException("The radius must be larger or equal to the lower bound");
		this.radius = radius;
		this.setActionPointsCurrent(this.getActionPointsCurrent());
	}
	
	/**
	 * Get the current lower bound on the radius of the worm
	 * 
	 * @return the current lower bound on the radius
	 * 		 | result == this.radiusLowerBound
	 */
	@Basic
	public double getRadiusLowerBound() {
		return radiusLowerBound;
	}
	
	/**
	 * Set the lower bound on the radius of the worm. This method is currently private, since the 
	 * specification doesn't require it (yet) to exist publicly.
	 * 
	 * @param 	radiusLowerBound
	 * 			the lower bound to set to
	 * @post 	this worms lower bound is set to radiusLowerBound
	 * 		  | this.getRadiusLowerBound() == radiusLowerBound
	 * @throws  IllegalArgumentException
	 * 			the lower bound must be a valid number
	 * 		  | radiusLowerBound == Float.NaN
	 * 			the lower bound cannot be negative
	 * 		  | radiusLowerBound < 0
	 */
	@Raw
	private void setRadiusLowerBound(double radiusLowerBound) throws IllegalArgumentException{
		if(Double.isNaN(radiusLowerBound))
			throw new IllegalArgumentException("The lower bound on the radius must be a valid number");
		if(radiusLowerBound < 0)
			throw new IllegalArgumentException("The lower bound on the radius cannot be negative");
		this.radiusLowerBound = radiusLowerBound;
	}
	
	/**
	 * Get the current mass of the worm
	 * 
	 * @return result == 1062 * (4/3)*Math.PI*Math.pow(this.getRadius(), 3)
	 */
	public double getMass() {
		return 1062 * (4.0/3.0)*Math.PI*Math.pow(this.getRadius(), 3);
	}
	
	/**
	 * Returns the current amount of action points.
	 * 
	 * @return the current amount of action points
	 * 		 | result = this.actionPointsCurrent
	 */
	@Basic
	public int getActionPointsCurrent() {
		return actionPointsCurrent;
	}
	
	/**
	 * Sets the current amount of action points.
	 * 
	 * @param 	actionPointsCurrent
	 *			the amount of action points to set to
	 * @post 	If the given amount of action points are not negative 
	 * 			and smaller than the maximum amount, then this worms action points
	 * 			are equal to the given amount
	 * 		  | if(0 <= actionPointsCurrent && actionPointsCurrent <= this.getActionPointsMax())
	 * 		  | 	this.actionPointsCurrent = actionPointsCurrent
	 * @post	If the given amount of action points is negative, this worms
	 * 			action points are equal to 0
	 * 		  | if (actionPointsCurrent < 0)\
	 * 		  | 	this.actionPointsCurrent = 0
	 * @post 	If the given amount of action points exceeds the maximum amount
	 *			then this worms action points are equal to the maximum amount
	 *		  | if(actionPointsCurrent > this.getActionPointsMax())
	 *		  | 	this.actionPointsCurrent = this.getActionPointsMax()
	 */
	@Raw
	public void setActionPointsCurrent(int actionPointsCurrent) {
		if(actionPointsCurrent < 0)
			this.actionPointsCurrent = 0;
		else if(actionPointsCurrent > this.getActionPointsMax())
			this.actionPointsCurrent = this.getActionPointsMax();
		else
			this.actionPointsCurrent = actionPointsCurrent;
	}
	
	/**
	 * Returns the maximum amount of Action points.
	 * 
	 * @return 	The max amount of AP
	 * 			| result = round(this.getMass())
	 */
	public int getActionPointsMax() {
		return (int) Math.round(this.getMass());
	}
}
