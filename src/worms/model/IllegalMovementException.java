package worms.model;

/**
 * An exception class for when a worm would make an illegal movement
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/oo-worms/overview
 *
 */
public class IllegalMovementException extends RuntimeException{

	private static final long serialVersionUID = -3644650050433949878L;

	public IllegalMovementException(String message) {
		super(message);
	}

	public IllegalMovementException(Throwable cause) {
		super(cause);
	}

	public IllegalMovementException(String message, Throwable cause) {
		super(message, cause);
	}

}
