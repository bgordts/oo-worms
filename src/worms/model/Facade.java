package worms.model;

/**
 * A class implementing the interface between the GUI and Worm
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/oo-worms/overview
 *
 */
public class Facade implements IFacade{

	@Override
	public Worm createWorm(double x, double y, double direction, double radius,
			String name) {
		try{
			return new Worm(x, y, direction, radius, name);
		} catch(IllegalArgumentException ex){
			throw new ModelException(ex.getMessage());
		}
	}

	@Override
	public boolean canMove(Worm worm, int nbSteps) {
		return worm.canMove(nbSteps);
	}

	@Override
	public void move(Worm worm, int nbSteps) {
		try{
			worm.move(nbSteps);
		} catch(IllegalMovementException ex){
			throw new ModelException(ex.getMessage());
		}
	}

	@Override
	public boolean canTurn(Worm worm, double angle) {
		return worm.canTurn(angle);
	}

	@Override
	public void turn(Worm worm, double angle) {
		if(angle == Float.NaN || !worm.canTurn(angle))
			throw new ModelException("The worm cannot turn over this angle");
		worm.turn(angle);
		
	}

	@Override
	public void jump(Worm worm) {
		try{
			worm.jump();
		} catch(IllegalMovementException ex){
			throw new ModelException(ex.getMessage());
		}
		
	}

	@Override
	public double getJumpTime(Worm worm) {
		return worm.jumpTime();
	}

	@Override
	public double[] getJumpStep(Worm worm, double t) {
		return worm.jumpStep(t);
	}

	@Override
	public double getX(Worm worm) {
		return worm.getPositionX();
	}

	@Override
	public double getY(Worm worm) {
		return worm.getPositionY();
	}

	@Override
	public double getOrientation(Worm worm) {
		return worm.getDirection();
	}

	@Override
	public double getRadius(Worm worm) {
		return worm.getRadius();
	}

	@Override
	public void setRadius(Worm worm, double newRadius) {
		try{
			worm.setRadius(newRadius);
		} catch(IllegalArgumentException ex){
			throw new ModelException(ex.getMessage());
		}
	}

	@Override
	public double getMinimalRadius(Worm worm) {
		return worm.getRadiusLowerBound();
	}

	@Override
	public int getActionPoints(Worm worm) {
		return worm.getActionPointsCurrent();
	}

	@Override
	public int getMaxActionPoints(Worm worm) {
		return worm.getActionPointsMax();
	}

	@Override
	public String getName(Worm worm) {
		return worm.getName();
	}

	@Override
	public void rename(Worm worm, String newName) {
		try{
			worm.setName(newName);
		} catch(IllegalArgumentException ex){
			throw new ModelException(ex.getMessage());
		}
	}

	@Override
	public double getMass(Worm worm) {
		return worm.getMass();
	}

}
